# Signature of the current package.
m4_define([AT_PACKAGE_NAME],
  [abrt])
m4_define([AT_PACKAGE_TARNAME],
  [abrt])
m4_define([AT_PACKAGE_VERSION],
  [2.10.9])
m4_define([AT_PACKAGE_STRING],
  [abrt 2.10.9])
m4_define([AT_PACKAGE_BUGREPORT],
  [crash-catcher@fedorahosted.org])
m4_define([AT_PACKAGE_URL],
  [])
